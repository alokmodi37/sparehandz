<?php

use Illuminate\Database\Seeder;
use Tymon\JWTAuth\JWTAuth as JWTAuth;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(JWTAuth $JWTAuth)
    {
    	$user = (factory(App\User::class)->create());
        $token = $JWTAuth->fromUser($user);

        App\User::where('id', $user->id)
                ->update(['api_token' => $token]);
        
    }
}
